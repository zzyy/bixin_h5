import http from '@/utils/axios'
import api from '@/api'
import { getStore, setStore } from '@/utils/storage'
import Vue from 'vue'
import { WechatPlugin } from 'vux'
// 使用微信 jssdk
Vue.use(WechatPlugin)

const wx = (() => {
  return navigator.userAgent.toLowerCase().indexOf('micromessenger') !== -1
})()

if (wx) {
  // 注意这里的url，一定要这样写，也就是动态获取，不然也不会成功的。
  // let url = location.href.split('?')[0].split('#')[0]
  let url = location.href.split('#')[0]
  let param = {
    url: api.shareData.url + '?url=' + encodeURIComponent(url),
    method: api.shareData.method
  }
  http(param).then(
    res => {
      if (res.code === 0) {
        let wxData = res.data
        if (wxData.signature !== null) {
          if (!getStore('openId')) {
            // 判断url是否包含code字段，如果长度为1，则表示不存在code，那么跳转到微信授权链接
            if (location.href.split('code').length === 1) {
              location.href = `https://open.weixin.qq.com/connect/oauth2/authorize?
                appid=${wxData.appId}&redirect_uri=${encodeURIComponent(wxData.url)}
                &response_type=code&scope=snsapi_userinfo&state=state#wechat_redirect`
            } else {
              let code = getQueryString('code')
              let tParam = {
                url: api.getAccessToken.url + '?code=' + code,
                method: api.getAccessToken.method
              }
              http(tParam).then(
                res => {
                  if (res.code === 0) {
                    setStore('openId', res.data.openid)
                  }
                }
              )
            }
          }
          Vue.wechat.config({
            debug: false,
            appId: wxData.appId,
            timestamp: wxData.timestamp,
            nonceStr: wxData.nonceStr,
            signature: wxData.signature,
            jsApiList: ['checkJsApi', 'onMenuShareTimeline', 'onMenuShareAppMessage', 'onMenuShareQQ', 'onMenuShareQZone']
          })

          let title = '安全守护，我向南京比个心，全民补贴来啦！'
          let desc = '品牌联合，多种补贴券、奖品放送！打卡南京，点击领取吧！'
          let img = 'https://dhsz.fangxiaopin.cn/images/finger-heart.jpg'

          // 分享给朋友、QQ、微博、朋友圈
          let shareData = {
            imgUrl: img,
            title: title,
            desc: desc,
            link: isIosOrAndroid() === 'android' ? location.href.split('?')[0] : url
          }
          Vue.wechat.ready(() => {
            Vue.wechat.onMenuShareTimeline(shareData)
            Vue.wechat.onMenuShareAppMessage(shareData)
            Vue.wechat.onMenuShareQQ(shareData)
            Vue.wechat.onMenuShareQZone(shareData)

            Vue.wechat.error((res) => {
              alert(res.errMsg)
            })
          })
        }
      }
    }
  )
}

function getQueryString (name) {
  let reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i')
  let r = window.location.search.substr(1).match(reg)
  if (r != null) {
    return unescape(r[2])
  }
  return null
}

// 判断安卓或ios
function isIosOrAndroid () {
  let u = navigator.userAgent
  let isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1 // android终端
  let isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/) // ios终端
  let isStr = ''
  if (isAndroid) {
    isStr = 'android'
  }
  if (isiOS) {
    isStr = 'ios'
  }
  return isStr
}
