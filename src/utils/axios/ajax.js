import axios from 'axios'
import Vue from 'vue'
import { ToastPlugin } from 'vux'
import { getStore } from '../storage'

Vue.use(ToastPlugin)

let vm = new Vue()

const root = process.env.API_URL

// 请求
axios.interceptors.request.use(
  config => {
    let openId = getStore('openId')
    config.headers.openId = openId
    return config
  },
  error => {
    return Promise.reject(error)
  }
)

// 请求完成
axios.interceptors.response.use(
  response => {
    return response
  },
  error => {
    return Promise.resolve(error.response)
  }
)

// 成功回调
function successState (res) {
  if (res.data.code === 500) {
    vm.$vux.toast.show({
      type: 'text',
      position: 'middle',
      width: 'auto',
      text: `<span style="font-size: 0.4rem;">${res.data.msg}</span>`
    })
  }
  if (res.data.code === 600 || res.data.code === 700) {
    // MessageBox('提示', res.data.message)
  }
  // if (res.data.code === 401) {
  //   router.push('login')
  // }
}

// 失败回调
function errorState (err) {
  if (err && (err.status === 200 || err.status === 304 || err.status === 400 || err.status === 404)) {
    return err
  } else {
    // MessageBox('提示', '网络异常')
  }
}

const httpServer = (opts, data) => {
  let Public = {}
  let httpDefaultOpts = {
    method: opts.method,
    url: opts.url,
    timeout: 60000,
    baseURL: root,
    params: Object.assign(Public, data),
    data: Object.assign(Public, data),
    headers: opts.method === 'get' ? {
      'X-Requested-With': 'XMLHttpRequest',
      'Accept': 'application/json',
      'Content-Type': 'application/json;charset=UTF-8'
    } : {
      'X-Requested-with': 'XMLHttpRequest',
      'Content-Type': 'application/json;charset=UTF-8'
    }
  }

  if (opts.method === 'get') {
    delete httpDefaultOpts.data
  } else {
    delete httpDefaultOpts.params
  }

  return new Promise((resolve, reject) => {
    axios(httpDefaultOpts)
      .then(res => {
        successState(res)
        resolve(res.data)
      })
      .catch(err => {
        errorState(err)
        reject(err)
      })
  })
}

export default httpServer
