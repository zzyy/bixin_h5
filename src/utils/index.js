import ajax from './axios'
import { setStore, getStore, removeStore } from './storage'
export {
  ajax,
  setStore,
  getStore,
  removeStore
}
