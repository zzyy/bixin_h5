const apiModule = {
  // 比心查询
  heartDay: {
    url: '/app/fingerHeart/list',
    method: 'get'
  },
  // 用户注册
  register: {
    url: '/app/user/register',
    method: 'post'
  },
  // 用户信息
  infoUser: {
    url: '/app/user/info',
    method: 'get'
  },
  // 短信-验证码
  getCaptcha: {
    url: '/app/user/captcha',
    method: 'get'
  },
  // 比心操作
  clickHeart: {
    url: '/app/fingerHeart/save',
    method: 'post'
  },
  // 获取当前用户比心状态
  stateHeart: {
    url: '/app/fingerHeart/day',
    method: 'get'
  },
  // 参与抽奖
  goDraw: {
    url: '/app/luck/draw',
    method: 'post'
  },
  // 个人奖品查询
  infoDraw: {
    url: '/app/luck/info',
    method: 'get'
  },
  // 分享数据
  shareData: {
    url: '/app/share/shareData',
    method: 'get'
  },
  // 通过code换取网页授权access_token
  getAccessToken: {
    url: '/app/share/getAccessToken',
    method: 'get'
  }
}

export default apiModule
