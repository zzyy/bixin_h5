// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import 'lib-flexible/flexible'
// import VConsole from 'vconsole'
import { ajax, setStore, getStore, removeStore } from '@/utils'
import api from './api'
import {
  WechatPlugin,
  XInput,
  Spinner,
  ToastPlugin,
  LoadingPlugin
} from 'vux'
require('@/assets/js/share')
// 使用微信 jssdk
Vue.use(WechatPlugin)
Vue.use(ToastPlugin)
Vue.use(LoadingPlugin)
Vue.component('x-input', XInput)
Vue.component('spinner', Spinner)

// let vconsole = new VConsole()
// console.log(vconsole)

// 移除移动端页面点击延迟
const FastClick = require('fastclick')
FastClick.attach(document.body)

Vue.prototype.$http = ajax
Vue.prototype.$api = api
Vue.prototype.setStore = setStore
Vue.prototype.getStore = getStore
Vue.prototype.removeStore = removeStore

Vue.config.productionTip = false

// 统计代码
/* eslint-disable */
router.beforeEach((to, from, next) => {
  setTimeout(() => {
    let _mtac = {}
    let mta = document.createElement('script')
    mta.src = '//pingjs.qq.com/h5/stats.js?v2.0.4'
    mta.setAttribute('name', 'MTAH5')
    mta.setAttribute('sid', '500714235')
    let s = document.getElementsByTagName('script')[0]
    s.parentNode.insertBefore(mta, s)
  }, 50)
  next()
})
/* eslint-enable */

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
